# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias      l='ls --color=always -F'
    alias     ll='ls --color=always -F -lh'
    alias      L='ls --color=always -F     -L'
    alias     LL='ls --color=always -F -lh -L'
    alias     la='ls --color=always -F        -a'
    alias    lla='ls --color=always -F -lh    -a'
    alias    ll.='ls --color=always -F -lh    -d .'
    alias  ldots='ls --color=always -F        -A --ignore=\*'
    alias lldots='ls --color=always -F -lh    -A --ignore=\*'


    alias  grep='grep  --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

alias rmtil='rm *~'

alias dirs="builtin dirs -v"
function pushd {
    builtin pushd "$@" > /dev/null
    dirs
}
function popd {
    builtin popd "$@" > /dev/null
    dirs
}
function rotd {
    pushd -0
}

################################################################
#hg
alias hglog='hg log --template "changeset:{rev}:{node|short} ({phase})\ntags:{tags}\nuser:{author}\ndate:{date|isodate}\nsummary:{desc}\n\n"'

################################################################

function run() {
    nohup "$@" >> $HOME/run-nohup.out &
    disown
}
complete -F _complete run

################################################################

function desktop {
    activity=$(xprop -id $WINDOWID | sed -rn -e  's/^_KDE_NET_WM_ACTIVITIES\(STRING\) = "(.+)"/\1/pg')
    desktop=$(xprop -id $WINDOWID | sed -rn -e  's/^_NET_WM_DESKTOP\(CARDINAL\) = (.+)/\1/pg')
    echo "${desktop}-${activity}"
}

alias edit='emacsclient --alternate-editor="" --no-wait $*'
function it {
    #this method gives a differant emacs server to each X11 virtual desktop
    desktop=$(desktop)
    if test "z${desktop}" != "z"
    then
        server="desktop${desktop}"
    else
        server="server" #use this server if can't find virtual desktop
    fi

    echo server=$server

    emacsclient --no-wait -s "${server}" "$@"
    if test  "z$PIPESTATUS" != "z0"
    then
        lisp="(setq server-name '\"${server}\")"
	emacs --daemon --eval "$lisp"
        emacsclient --no-wait -s "${server}" "$@"
    fi
}

################################################################
function emacsdoit() {
    emacsclient  --eval "$@"
}

function emacs_setcompilercommand() {
    echo "setting compile-command"
    emacsdoit "(setq compile-command \"cd $(pwd); ./make.bat\")"
    echo "setting grep-find-command"
    emacsdoit "(setq grep-find-command \"find $(pwd) \\\\( \\\\( -name _svn -o -iname object \\\\) -prune \\\\) -o -type f \\\\( -iname '*.cpp' -o -iname '*.c' -o -iname '*.h' \\\\) -print0 | xargs -0 -e grep -inH -E \")" 
    echo "loading highlight config"
    emacsdoit "(ctypes-read-file \"$(pwd)/emacs.ctypes\")"
}

################################################################
#docker
alias docker="sudo --group docker docker"

alias dockviz="docker run --name dockviz --rm  -v /var/run/docker.sock:/var/run/docker.sock nate/dockviz"
function _dockvizdot {
    dockviz "$@" --dot | dot -Tx11
}
alias dockviz-containers="_dockvizdot containers"
alias dockviz-images="_dockvizdot images -l"

################################################################
#ipfs via docker
function start-ipfs {
    staging="/tmp/ipfs-docker-staging"
    data="/tmp/ipfs-docker-data"

    mkdir -p "${staging}"
    mkdir -p "${data}"

    ipfsId="$(
	sudo --group docker \
            docker run -d --name ipfs-node \
	        -v "${staging}":/export -v "${data}":/data/ipfs \
	        -p 8080:8080 -p 4001:4001 -p 127.0.0.1:5001:5001 \
	        jbenet/go-ipfs:latest)"
    echo $ipfsId
}

alias ipfs="docker exec -t ipfs-node ipfs"

################################################################
#golang
alias go='docker run --rm -v "$GOPATH":/go -v "$PWD":/usr/src/myapp -w /usr/src/myapp golang:1.11 go'





